package categories;
import entities.Product;
import entities.Site;

abstract class Category  extends Site {


    private int id;
    private String name;

    public Category(String name, String url) {
        super(name, url);
    }
    public Category(){}

    abstract void addProduct(Product p);
    abstract void removeProduct(Product p);
    abstract void updateProduct(Product p);
    abstract Product getProduct(int id);
}
