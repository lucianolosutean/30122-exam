package entities;

public class Toy extends Product {

    public Toy() {
        super.getId();
        super.getName();
        super.getPrice();
    }


    @Override
    public void markForSale() {
        setForSale(true);

    }

    private void setForSale(boolean b) {
    }

    @Override
    public void retireFromSale() {
        setForSale(false);

    }


}
