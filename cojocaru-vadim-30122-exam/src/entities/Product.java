package entities;


import actions.RetireProduct;
import actions.SellProduct;


abstract public class Product extends ProductDetails implements SellProduct, RetireProduct {
    private boolean forSale;
    private int categoryId;

   public boolean isForSale(){
        return forSale;
   }

   public void setForSaele(boolean forSale) {
       this.forSale=forSale;
   }

   public int getCategoryId(){
       return categoryId;
   }

   public void setCategoryId(int categoryId) {
       this.categoryId=categoryId;
   }

    public abstract void markForSale();

    public abstract void retireFromSale();
}
