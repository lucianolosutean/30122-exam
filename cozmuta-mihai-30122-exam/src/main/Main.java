package main;


import entities.Site;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Site mySite = new Site("DreamLand", "https://www.dreamland.com");


        //Our first menu, the one which lets you see the categories and the products within them
        System.out.println("Hello, welcome to " + mySite.getName() + " (" + mySite.getUrl() + ").");
        System.out.println("Our categories are: ");
        System.out.println("1)Books;\n2)Video Games;\n3)Writing instruments.\n");
        String firstChoice = "a string";

        System.out.println("Please select a category to see its specific products. If you want to go forward " +
                "and do more stuff, just type <<exit>>");

        while (!firstChoice.contentEquals("exit")) {
            firstChoice = in.nextLine();
            switch (firstChoice) {
                case "1":
                    System.out.println("The Lord of the Rings\nA song of ice and fire");
                    break;
                case "2":
                    System.out.println("GTA V\nCounter Strike");
                    break;
                case "3":
                    System.out.println("Pencil\nPen\nCarioca");

            }
            if (!firstChoice.contentEquals("exit"))
                System.out.println("--Please choose another category or type <<exit>>--");
        }
        //

        //The second menu, the one which lets you do actions to the products
        int choice = 1;
        while (choice != 0) {
            System.out.println("Please select your choice:");

            System.out.println("1)Add a new product to a category;\n2)Delete a product from a category;" +
                    "\n3)Edit an existing product;\n4)Put a product up for sale;\n5)Take a product off market;\n" +
                    "0)Exit from the menu and close the app\n");

            choice = in.nextInt();
            switch (choice) {
                case 1:

            }
        }
        System.out.println("Thank you for visting our webiste, hope you enjoyed your journey. Bye! :*");


    }
}
