package entities;

public class Site {

    private String name;
    private String url;

    public Site(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void initCategories() {
    }
}
