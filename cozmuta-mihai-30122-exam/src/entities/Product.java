package entities;

import actions.*;
import categories.Category;


public abstract class Product extends ProductDetails implements RetireProduct, SellProduct {
    private boolean forSale;
    private int categoryId;
    private Category myCategory;


    public abstract void markForSale();

    public abstract void retireFromSale();


}
