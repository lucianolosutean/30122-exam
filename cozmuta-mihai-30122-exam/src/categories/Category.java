package categories;


import entities.Product;
import entities.Site;

public abstract class Category {
    private int id;
    private String name;
    private Site mySite;
    public Product myProduct;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void addProduct(Product p) {

    }

    public void removeProduct(Product p) {
    }

    public void updateProduct(Product p) {

    }

    public Product getProduct(int id) {
        return myProduct;
    }
}
