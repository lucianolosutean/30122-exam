package entities;

import categories.BookCategory;
import categories.Category;
import categories.ToyCategory;

import java.util.ArrayList;

public class Site {
    private String name;
    private String url;
    private ArrayList<Category> categories=new ArrayList<>();
    public Site(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void InitCategories(){
        Category bc=new BookCategory(3,"Beltristica");
        Category tc=new ToyCategory(2,"Plus");
        categories.add(bc);
        categories.add(tc);
        System.out.println("site:"+this.getName()+" cu url"+this.getUrl()+" cu Categoriile");
       /* for(Category x ; categories){
            System.out.println(x.getName()+"  ");                                   //neintializat x?

        }*/
    }
}
