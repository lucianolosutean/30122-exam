package entities;

import actions.RetireProduct;
import actions.SellProduct;

public abstract class Product extends ProductDetails implements SellProduct, RetireProduct {
    private boolean forSale;
    private int categoryId;


    public abstract void markForSale();
    public abstract void retireFromSale();

    public abstract int getId();
}
