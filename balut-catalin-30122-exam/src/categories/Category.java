package categories;

import entities.Product;

public abstract class Category {
    private int id;
    private String name;

    public Category(){};

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void addProduct(){};
    public void removeProduct(){};
    public void updateProduct(){};
    /*public Product(int id){
        return Product
    }*/
}
