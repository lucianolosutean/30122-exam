package categories;

import entities.Product;
import entities.Toy;

import java.util.ArrayList;

public class ToyCategory extends Category {
    private ArrayList<Toy> toys =new ArrayList<>();
    public ToyCategory() {
    }

    public ToyCategory(int id, String name) {
        super(id, name);
    }

    public void addProduct(Product p){};
    public void removeProduct(Product p){
        removeProduct(p);
    };
    public void updateProduct(Product p){};

    public Product getProduct(int id){
        for (Toy t:toys){
            if(t.getId()==id){
                return t;
            }
        }
        return null;
    }
}
