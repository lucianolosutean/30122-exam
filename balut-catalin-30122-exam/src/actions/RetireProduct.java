package actions;

public interface RetireProduct {
    public void markForSale();
}
