package entities;

public class Book extends Product{


    public Book(int id, String name, int price, boolean forSale, int categoryId) {
        super(id, name, price, forSale, categoryId);
    }

    @Override
    public void markForSale() {
        super.markForSale();
    }

    @Override
    public void retireFromSale() {
        super.retireFromSale();
    }
}
