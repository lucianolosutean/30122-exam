package entities;

public class ProductImpl extends Product {
    public ProductImpl(int id, String name, int price, boolean forSale, int categoryId) {
        super(id, name, price, forSale, categoryId);
    }
}
