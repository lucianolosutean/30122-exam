package entities;


import actions.RetireProduct;
import actions.SellProduct;

public abstract class Product extends ProductDetails implements SellProduct,RetireProduct{



    private boolean forSale;
    protected int categoryId;

    public Product(int id, String name, int price, boolean forSale,int categoryId) {
        super(id, name, price);
        this.forSale = forSale;
        this.categoryId = categoryId;
    }



    public void markForSale(){
        if(this.forSale = !forSale){
            System.out.println("Not for Sale: " + this.categoryId);
        }else {
            System.out.println("For Sale: " + this.categoryId);
        }


    }

    public void retireFromSale(){
        if(this.forSale = !forSale){
            System.out.println("For sale: " + categoryId);
        }else{

            System.out.println("Retire product: " + categoryId);
        }

    }


}
