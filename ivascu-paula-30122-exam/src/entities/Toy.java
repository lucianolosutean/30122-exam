package entities;

public class Toy extends Product{

    public Toy(int id, String name, int price, boolean forSale, int categoryId) {
        super(id, name, price, forSale, categoryId);
    }

    @Override
    public void markForSale() {
        super.markForSale();
    }


    @Override
    public void retireFromSale() {
        super.retireFromSale();
    }

}
