package entities;

class ProductDetails {

    private int id;
    private String name;
    private int price;

    public ProductDetails(int id, String name, int price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}
