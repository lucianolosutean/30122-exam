package categories;

import entities.Product;
import entities.Toy;
import java.util.ArrayList;

public class ToyCategory extends Category {

    private ArrayList<Toy> toys = new ArrayList<Toy>();

    @Override
    public void addProduct(Product p) {
        super.addProduct(p);
    }

    @Override
    public void removeProduct(Product p) {
        super.removeProduct(p);
    }

    @Override
    public void updateProduct(Product p) {
        super.updateProduct(p);
    }

    @Override
    public Product getProduct(int id) {
        return super.getProduct(id);
    }
}
