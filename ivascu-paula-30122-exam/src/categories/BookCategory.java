package categories;

import entities.Book;
import entities.Product;
import java.util.ArrayList;

public class BookCategory extends Category {

    private ArrayList<Book> books = new ArrayList<Book>();

    public BookCategory(ArrayList<Book> books) {
        super();
        this.books = books;
    }

    @Override
    public void addProduct(Product p) {
        super.addProduct(p);
    }

    @Override
    public void removeProduct(Product p) {
        super.removeProduct(p);
    }

    @Override
    public void updateProduct(Product p) {
        super.updateProduct(p);
    }

    @Override
    public Product getProduct(int id) {
        return super.getProduct(id);
    }
}
