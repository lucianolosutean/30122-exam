package Entities;

public class Toy extends Product {

    public Toy(int id, String name, int price, boolean forSale, int categoryId) {
        super(id, name, price, forSale, categoryId);
    }

    @Override
    public void retireFromSale() {
        setForSale(false);
    }

    @Override
    public void markForSale() {
        setForSale(true);
    }
}
