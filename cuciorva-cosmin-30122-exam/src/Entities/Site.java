package Entities;

import Categories.BookCategory;
import Categories.Category;
import Categories.ToyCategory;

import java.util.ArrayList;

public class Site {
    ArrayList<Category> categories = new ArrayList<>();

    public ArrayList<Category> getCategories() {
        return categories;
    }

    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Site(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public void initCategories() {
        int i=1;
        Category book = new BookCategory(10, "SF");
        Category toy = new ToyCategory(20, "LEGO");
        categories.add(book);
        categories.add(toy);
        System.out.println("Numele site-ului " + this.getName() + " avand url-ul " + this.getUrl()+" si urmatoarele categorii: ");
        for (Category c:categories) {
            System.out.println(i+"."+c.getName()+" ");
            i++;
        }
        System.out.println("");
    }
}
