package Entities;

import Actions.RetireProduct;
import Actions.SellProduct;

abstract public class Product extends ProductDetails implements SellProduct, RetireProduct {
    private boolean forSale;
    private int categoryId;

    public Product(int id, String name, int price, boolean forSale, int categoryId) {
        super(id, name, price);
        this.forSale = forSale;
        this.categoryId = categoryId;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }


    abstract public void retireFromSale();


    abstract public void markForSale();
}
