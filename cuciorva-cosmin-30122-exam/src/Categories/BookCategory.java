package Categories;

import Entities.Book;
import Entities.Product;

import java.util.ArrayList;
import java.util.Scanner;

public class BookCategory extends Category {
    ArrayList<Book> books = new ArrayList<>();

    public BookCategory(int id, String name) {
        super(id, name);
    }

    @Override
    public void addProduct(Product p) {
        books.add((Book) p);
    }

    @Override
    public void removeProduct(Product p) {
        books.remove(getProduct(p.getId()));
    }

    @Override
    public void updateProduct(Product p) {
        int optiune = 0;
        Scanner scanner = new Scanner(System.in);
        for (Book b : books) {
            if (b.getId() == p.getId()) {
                while (optiune < 7) {
                    System.out.println("1.Modificati numele produsului\n2.Modificati pretul produsului\n3.Exit");
                    optiune = scanner.nextInt();
                    switch (optiune) {
                        case 1:
                            System.out.println("Introduceti noul nume:");
                            p.setName(scanner.next());
                            break;
                        case 2:
                            System.out.println("Introduceti noul pret al produsului");
                            p.setPrice(scanner.nextInt());
                            break;
                        case 3:
                            return;
                        default:
                            System.out.println("Introduceti o optiune valida!");
                    }
                }
            }
        }
    }

    @Override
    public Product getProduct(int id) {
        for (Book b : books) {
            if (b.getId() == id) {
                return b;
            }
        }
        return null;
    }

    @Override
    public void afisare() {
        for (Book b : books) {
            System.out.println("ID " + b.getId() + " nume " + b.getName() + " pret " +b.getPrice());
        }
    }

}
