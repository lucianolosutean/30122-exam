package Categories;

import Entities.Product;
import Entities.Toy;

import java.util.ArrayList;
import java.util.Scanner;

public class ToyCategory extends Category {
    ArrayList<Toy> toys = new ArrayList<>();

    public ToyCategory(int id, String name) {
        super(id, name);
    }

    @Override
    public void addProduct(Product p) {
        toys.add((Toy) p);
    }

    @Override
    public void removeProduct(Product p) {
        toys.remove(getProduct(p.getId()));
    }

    @Override
    public void updateProduct(Product p) {
        int optiune = 0;
        Scanner scanner = new Scanner(System.in);
        for (Toy t : toys) {
            if (t.getId() == p.getId()) {
                while (optiune < 7) {
                    System.out.println("1.Modificati numele produsului\n2.Modificati pretul produsului\n3.Exit");
                    optiune = scanner.nextInt();
                    switch (optiune) {
                        case 1:
                            System.out.println("Introduceti noul nume:");
                            String name = scanner.next();
                            p.setName(name);
                            break;
                        case 2:
                            System.out.println("Introduceti noul pret al produsului");
                            int price = scanner.nextInt();
                            p.setPrice(price);
                            break;
                        case 3:
                            return;
                        default:
                            System.out.println("Introduceti o optiune valida!");
                    }
                }
            }
        }

    }

    @Override
    public Product getProduct(int id) {
        for (Toy t : toys) {
            if (t.getId() == id) {
                return t;
            }
        }
        return null;
    }
    @Override
    public void afisare() {
        for (Toy t : toys) {
            System.out.println("ID " + t.getId() + " nume " + t.getName() + " pret " +t.getPrice());
        }
    }
}
