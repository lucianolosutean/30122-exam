package Actions;

public interface SellProduct {
    void markForSale();
}
