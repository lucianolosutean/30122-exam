package Main;

import Categories.BookCategory;
import Categories.ToyCategory;
import Entities.Book;
import Entities.Product;
import Entities.Site;
import Entities.Toy;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int optiune = 0;
        Site site = new Site("eMAG", "https://www.emag.ro");
        site.initCategories();


        BookCategory book = (BookCategory) site.getCategories().get(0);
        ToyCategory toy = (ToyCategory) site.getCategories().get(1);
        // in caz ca as fi facut cu fisier aici ar fi aparut inregistrarile produselor existente
        // book.afisare();
        // toy.afisare();

        int idBook = site.getCategories().get(0).getId();
        int idToy = site.getCategories().get(1).getId();

        while (optiune < 7) {
            System.out.println("1.Adauga un produs in categorie\n2.Sterge un produs din categorie\n3.Editeaza un produs existent\n4.Pune un produs la vanzare\n5.Retrage un produs de la vanzare\n6.Exit\nOptiune:");
            optiune = scanner.nextInt();
            switch (optiune) {
                case 1:
                    System.out.println("ID-ul categoriei BOOK este 10,respectiv 20 pentru categoria TOY");
                    System.out.println("Numele produsului=");
                    String name = scanner.next();
                    System.out.println("Pretul produsului=");
                    int pret = scanner.nextInt();
                    System.out.println("Introduceti id-ul produsului");
                    int id = scanner.nextInt();
                    System.out.println("For sale=(true/false)");
                    boolean forSale = scanner.nextBoolean();
                    System.out.println("ID-ul categoriei in care doriti sa-l adaugati:");
                    int idCat = scanner.nextInt();
                    if (idCat == idBook) {
                        Product p = new Book(id, name, pret, forSale, idCat);
                        book.addProduct(p);
                        book.afisare();
                    } else if (idCat == idToy) {
                        Product p = new Toy(id, name, pret, forSale, idCat);
                        toy.addProduct(p);
                        toy.afisare();
                    } else {
                        System.out.println("Nu exista id-ul categoriei pe care l-ati ales");
                    }
                    System.out.println("Adaugarea a avut loc!");
                    break;
                case 2:
                    System.out.println("Introduceti id-ul produsului");
                    int idDel = scanner.nextInt();
                    System.out.println("ID-ul categoriei in care doriti sa stergeti:");
                    int idCatDel = scanner.nextInt();
                    if (idCatDel == idBook) {
                        if (book.getProduct(idDel) != null) {
                            book.removeProduct(book.getProduct(idDel));
                            book.afisare();
                        } else {
                            System.out.println("Produsul nu exista!");
                        }
                    } else if (idCatDel == idToy) {
                        if (toy.getProduct(idDel) != null) {
                            toy.removeProduct(book.getProduct(idDel));
                            toy.afisare();
                        } else {
                            System.out.println("Produsul nu exista!");
                        }
                    } else {
                        System.out.println("Nu exista id-ul categoriei pe care l-ati ales");
                    }
                    System.out.println("Stergerea a avut loc!");
                    break;
                case 3:
                    System.out.println("Introduceti id-ul produsului");
                    int idUp = scanner.nextInt();
                    System.out.println("ID-ul categoriei in care doriti sa modificati:");
                    int idCatUp = scanner.nextInt();
                    if (idCatUp == idBook) {
                        if (book.getProduct(idUp) == null) {
                            System.out.println("Produsul pe care doriti sa-l modificati nu exista!");
                        } else {
                            book.updateProduct(book.getProduct(idUp));
                            System.out.println("Denumire carte=" + book.getProduct(idUp).getName() + " avand pretul " + book.getProduct(idUp).getPrice() + " RON");
                        }
                    } else if (idCatUp == idToy) {
                        if (toy.getProduct(idUp) != null) {
                            toy.updateProduct(toy.getProduct(idUp));
                            System.out.println("Denumire jucarie " + toy.getProduct(idUp).getName() + " avand pretul " + toy.getProduct(idUp).getPrice() + " RON");
                        } else {
                            System.out.println("Produsul pe care doriti sa-l modificati nu exista!");
                        }
                    } else {
                        System.out.println("Nu exista id-ul categoriei pe care l-ati ales");
                    }
                    System.out.println("Comanda 3 executata!");
                    break;
                case 4:
                    System.out.println("Introduceti id-ul produsului:");
                    int idFs = scanner.nextInt();
                    System.out.println("Id-ul categoriei de care apartine produsul pe care  doriti sa-l puneti la vanzare");
                    int idCatFs = scanner.nextInt();
                    if (idCatFs == idBook) {
                        if (book.getProduct(idFs) != null) {
                            if (book.getProduct(idFs).isForSale()) {
                                System.out.println("Produsul este deja la vanzare!");
                            } else {
                                book.getProduct(idFs).setForSale(true);
                                System.out.println("Numele=" + book.getProduct(idFs).getName() + " For Sale=" + book.getProduct(idFs).isForSale());
                            }
                        } else {
                            System.out.println("Produsul nu exista");
                        }
                    } else if (idCatFs == idToy) {
                        if (book.getProduct(idFs)!= null) {
                            if (toy.getProduct(idFs).isForSale()) {
                                System.out.println("Produsul este deja la vanzare!");
                            } else {
                                toy.getProduct(idFs).setForSale(true);
                                System.out.println(toy.getProduct(idFs).getName() + " For Sale=" + toy.getProduct(idFs).isForSale());
                            }
                        } else {
                            System.out.println("Produsul nu exista!");
                        }

                    } else {
                        System.out.println("Nu exista id-ul categoriei pe care l-ati ales!");
                    }
                    System.out.println("Comanda 4 executata!");
                    break;
                case 5:
                    System.out.println("Introduceti id-ul produsului:");
                    int idNfs = scanner.nextInt();
                    System.out.println("Id-ul categoriei de care apartine produsul pe care  doriti sa-l puneti la vanzare");
                    int idCatNfs = scanner.nextInt();
                    if (idCatNfs == idBook) {
                        if (book.getProduct(idNfs) != null) {
                            if (book.getProduct(idNfs).isForSale()) {
                                book.getProduct(idNfs).setForSale(false);
                                System.out.println("Numele=" + book.getProduct(idNfs).getName() + " For Sale=" + book.getProduct(idNfs).isForSale());
                            } else {
                                System.out.println("Produsul deja a fost retras de la vanzare!");
                            }
                        } else {
                            System.out.println("Produsul nu exista!");
                        }
                    } else if (idCatNfs == idToy) {
                        if (toy.getProduct(idNfs) != null) {
                            if (toy.getProduct(idNfs).isForSale()) {
                                toy.getProduct(idNfs).setForSale(false);
                                System.out.println(toy.getProduct(idNfs).getName() + " For Sale=" + toy.getProduct(idNfs).isForSale());
                            } else {
                                System.out.println("Produsul deja a fost retras de la vanzare!");
                            }
                        } else {
                            System.out.println("Produsul nu exista!");
                        }
                    } else {
                        System.out.println("ID-ul categoriei pe care l-ati ales nu exista!");
                    }
                    System.out.println("Comanda 5 executata!");
                    break;
                case 6:
                    return;
                default:
                    System.out.println("Alegeti o optiune valida!");
                    break;

            }
        }
    }
}
