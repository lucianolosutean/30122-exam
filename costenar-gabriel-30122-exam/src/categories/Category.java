package categories;
import entities.Product;


public abstract class Category {
    //atribute
    private int id;
    private String name;

    //metode
    abstract public void addProduct(Product p);

    abstract public void removeProduct(Product p);

    abstract public void updateProduct(Product p);

    abstract public Product getProduct(int id);



}
