package entities;



public abstract class Product extends ProductDetails{

    //atribute
    private boolean forSale;
    private int categoryId;

    //metode
    abstract public void markForSale();

    abstract public void retireFromSale();
}

