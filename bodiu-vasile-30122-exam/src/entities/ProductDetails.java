package entities;

public class ProductDetails extends Product {
    private int id;
    private String name;
    private int price;

    ProductDetails(String name, int id, int price) {
        this.name = name;
        this.id = id;
        this.price = price;
    }

    @Override
    public void markForSale() {

    }

    @Override
    public void retireFromSale() {

    }
}
