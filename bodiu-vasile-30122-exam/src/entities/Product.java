package entities;

import actions.RetireProduct;
import actions.SellProduct;

public abstract class Product implements RetireProduct, SellProduct {
    private boolean forSale;
    private int categoryId;

    protected Product(boolean forSale, int categoryId) {
        this.forSale = forSale;
        this.categoryId = categoryId;
}




    public abstract void markForSale();

    public abstract void retireFromSale();


}
