package entities;

import actions.RetireProduct;
import actions.SellProduct;

abstract public class Product extends ProductDetails implements SellProduct, RetireProduct {
    boolean forSale;
    int categoryId;

    abstract public void markForSale();
    abstract public void retireFromSale();
}
