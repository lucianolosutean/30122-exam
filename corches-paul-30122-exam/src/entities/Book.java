package entities;

public class Book extends Product{

    public Book(boolean f) {
        forSale=f;
    }

    public void markForSale(){
        forSale=true;
    }

    public void retireFromSale(){
        forSale=false;
    }
    public int getId(){
        return categoryId;
    }
}
