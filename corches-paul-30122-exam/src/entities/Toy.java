package entities;

public class Toy extends Product {
    public Toy(boolean f) {
        forSale=f;
    }

    public void markForSale(){
        forSale=true;
    }
    public void retireFromSale(){
        forSale=false;
    }
    public int getId(){
        return categoryId;
    }
}
