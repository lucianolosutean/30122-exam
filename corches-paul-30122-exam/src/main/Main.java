package main;

import categories.BookCategory;
import categories.ToyCategory;
import entities.Book;
import entities.Toy;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        String s="";
        String category;
        BookCategory bc=new BookCategory();
        ToyCategory tc= new ToyCategory();


        while(s!="e"||s!="E"){
            System.out.println("a- Adaugare produs nou in categorie");
            System.out.println("s- Stergere produs din categorie categorie");
            System.out.println("m- Editare produs existent");
            System.out.println("p- Punere produs la vanzare");
            System.out.println("r- Retragere produs din vanzare");
            System.out.println("e- Iesire");
            s=in.nextLine();
            System.out.println("Categoria: ");
            category=in.nextLine();

            if(s.equals("a")){
                if(category.equals("Toy")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Toy t=new Toy(true);
                    tc.addProduct(t);
                }
                if(category.equals("Book")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Book b=new Book(true);
                    bc.addProduct(b);
                }
            }
            if(s.equals("s")){
                if(category.equals("Toy")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Toy t=new Toy(true);
                    tc.removeProduct(t);
                }
                if(category.equals("Book")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Book b=new Book(true);
                    bc.removeProduct(b);
                }
            }
            if(s.equals("m")){
                if(category.equals("Toy")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Toy t=new Toy(true);
                    tc.updateProduct(t);
                }
                if(category.equals("Book")){
                    System.out.println("Introducere Id Categoriee:");
                    int cat=in.nextInt();
                    Book b=new Book(true);
                    bc.updateProduct(b);
                }
            }
            if(s.equals("p")){
                if(category.equals("Toy")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Toy t=(Toy)tc.getProduct(cat);
                    t.markForSale();


                }
                if(category.equals("Book")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Book b=(Book)tc.getProduct(cat);
                    b.markForSale();
                }
            }
            if(s.equals("r")){
                if(category.equals("Toy")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Toy t=(Toy)tc.getProduct(cat);
                    t.retireFromSale();
                }
                if(category.equals("Book")){
                    System.out.println("Introducere Id Categorie:");
                    int cat=in.nextInt();
                    Book b=(Book)tc.getProduct(cat);
                    b.retireFromSale();
                }
            }


        }
    }
}
