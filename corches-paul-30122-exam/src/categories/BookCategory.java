package categories;

import entities.Book;
import entities.Product;

import java.util.ArrayList;
import java.util.Iterator;

public class BookCategory extends Category {
    private ArrayList<Book> books = new ArrayList<Book>();

    public void addProduct(Product p) {
        Book b = (Book) p;
        books.add(b);

    }

    public void removeProduct(Product p) {
        books.remove(p);
    }

    public Product getProduct(int id) {
        Iterator it = books.iterator();
        while (it.hasNext()) {
            Book B = (Book) it.next();
            if (B.getId() == id) {
                return B;
            }
        }
        return null;

    }

    public void updateProduct(Product p) {
        books.remove(p);
        books.add((Book)p);
    }
}
