package categories;
import entities.Product;
import entities.Toy;

import java.util.ArrayList;
import java.util.Iterator;

public class ToyCategory extends Category {
    private ArrayList<Toy> toys = new ArrayList<Toy>();
    public void addProduct(Product p){
        Toy t=(Toy)p;
        toys.add(t);
    }

    public void removeProduct(Product p){
        toys.remove(p);
    }

    public void updateProduct(Product p){
        toys.remove(p);
        toys.add((Toy)p);
    }

    public Product getProduct(int id){
        Iterator it = toys.iterator();
        while (it.hasNext()) {
            Toy t = (Toy) it.next();
            if (t.getId() == id) {
                return t;
            }
        }
        return null;
    }


}
