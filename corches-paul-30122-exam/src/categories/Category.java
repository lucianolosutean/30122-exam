package categories;

import entities.Product;

abstract public class Category {
    private int id;
    private String name;


    abstract public void addProduct(Product P);
    abstract public void removeProduct(Product P);
    abstract public void updateProduct(Product P);
    abstract public Product getProduct(int id);
}
