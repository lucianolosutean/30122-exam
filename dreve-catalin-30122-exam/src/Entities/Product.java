package Entities;
import Actions.RetireProduct;
import Actions.SellProduct;

public abstract class Product extends ProductDetails implements SellProduct, RetireProduct{
    public boolean forSale;
    public int categoryId;
    public abstract void markForSale();
    public abstract void RetireFromSale();
}

