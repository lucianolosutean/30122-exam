package main;

import categories.BookCategory;
import categories.Category;
import categories.ToyCategory;
import entities.Book;
import entities.Product;
import entities.Toy;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        BookCategory b = new BookCategory();
        Product book1 = new Book();
        ToyCategory t = new ToyCategory();
        Product toy = new Toy();

        Scanner scanner = new Scanner(System.in);
        int ok = scanner.nextInt();

        System.out.println("1.Add a new product!");
        System.out.println("2.Remove a product!");
        System.out.println("3.Update a product!");
        System.out.println("4.Mark a product for sale!");
        System.out.println("5.Remove a product from sale!");

        switch (ok) {
            case 1:
                System.out.println("Add a new product");
                System.out.println("What is your product? 0 for book, 1 for toy");
                int i = scanner.nextInt();
                if (i == 0) {
                    b.addProduct(book1);
                }

                if (i == 1) {
                    t.addProduct(toy);
                }
                break;
            case 2:
                System.out.println("Remove a product");
                System.out.println("Choose a category 0 / 1");
                int j = scanner.nextInt();
                if (j == 0) {
                    b.removeProduct(book1);
                } else {
                    t.removeProduct(toy);
                }
                break;
            case 3:
                System.out.println("Update a product");
                System.out.println("Choose a category 0 / 1");
                int k = scanner.nextInt();
                if (k == 0) {
                    b.updateProduct(book1);
                } else {
                    t.removeProduct(toy);
                }
                break;
            case 4:
                System.out.println("Mark for sale!");
                System.out.println("Choose a category 0 / 1");
                int l = scanner.nextInt();
                if (l == 0) {
                    book1.setForSale(true);
                } else {
                    toy.setForSale(true);
                }
                break;
            case 5:
                System.out.println("Remove from sale!");
                System.out.println("Choose a category 0 / 1");
                int h = scanner.nextInt();
                if (h == 0) {
                    book1.setForSale(false);
                } else {
                    toy.setForSale(false);
                }


        }
    }
}
