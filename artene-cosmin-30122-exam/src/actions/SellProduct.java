package actions;

import java.io.*;

public interface SellProduct {

    void markForSale();
}
