package categories;

import entities.Product;
import entities.Toy;

import java.util.Scanner;

public class ToyCategory extends Category{

    public ToyCategory() {
        super.getId();
        super.getName();
    }
    Product toy = new Toy();
    Scanner scanner = new Scanner(System.in);

    @Override
    public void addProduct(Product p) {
        System.out.println("Introduce id:");
        int id2 = scanner.nextInt();
        toy.setId(id2);
        System.out.println("Introduce name:");
        String name1 = scanner.next();
        toy.setName(name1);
        System.out.println("Introduce price:");
        int price1 = scanner.nextInt();
        toy.setPrice(price1);
    }

    @Override
    public void removeProduct(Product p) {

    }

    @Override
    public void updateProduct(Product p) {

    }

    @Override
    public Product getProduct(int id) {
        return null;
    }
}
