package categories;

import entities.Product;
import entities.Site;

abstract public class Category extends Site {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    abstract public void addProduct(Product p);

    abstract public void removeProduct(Product p);

    abstract public void updateProduct(Product p);

    abstract public Product getProduct(int id);

}
