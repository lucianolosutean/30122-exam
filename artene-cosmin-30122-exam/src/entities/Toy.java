package entities;

public class Toy extends Product{

    public Toy() {
        super.getId();
        super.getName();
        super.getPrice();
    }

    @Override
    public void markForSale() {
        setForSale(true);
    }

    @Override
    public void retireFromSale() {
        setForSale(false);
    }
}
