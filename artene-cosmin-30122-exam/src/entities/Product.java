package entities;

import actions.RetireProduct;
import actions.SellProduct;
import categories.Category;

abstract public class Product extends ProductDetails implements SellProduct, RetireProduct {
    private boolean forSale;
    private int categoryId;

    public boolean isForSale() {
        return forSale;
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    abstract public void markForSale();

    abstract public void retireFromSale();
}
