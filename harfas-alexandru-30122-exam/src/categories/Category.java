package categories;

import java.util.*;
import entities.Product;

public abstract class Category {

    private int id;
    private String name;

    abstract void addProduct(Product p);
    abstract void removeProduct(Product p);
    abstract void updateProduct(Product p);
    abstract Product getProduct(int id);
}
