package entities;
import actions.*;

public abstract class Product extends ProductDetails implements RetireProduct, SellProduct{

    private boolean forSale;
    private int categoryId;

    abstract public void markForSale();
    abstract public void retireFromSale();

}
