package actions;

public interface RetireProduct {

    void markForSale();
}
