package Main;
import categories.ToyCategory;
import entities.Product;
import entities.Toy;

import javax.swing.*;
import java.io.IOException;

public class Main extends JFrame {

    JMenuBar mb;
    JMenu f;

    JMenuItem b1,b2,b3,b4,b5;
    public Main(){
        mb = new JMenuBar();
        f = new JMenu("Site");


        b1= new JMenuItem("Adugare un nou produs in categorie");
        b2= new JMenuItem("Stergerea unui produs din categorie ");
        b3= new JMenuItem("Editare un produs existent ");
        b4= new JMenuItem("Pune un produs la vanzare ");
        b5= new JMenuItem("retraga un produs de la vanzare");
        mb.add(f);
        f.add(b1);
        f.add(b2);
        f.add(b3);
        f.add(b4);
        f.add(b5);
        b1.addActionListener(e -> {    // aici fac adaugarea
            System.out.println("Ai adaugat un produs in categorie");
        });
        b2.addActionListener(e -> {
            // aici fac stergere
            System.out.println("Ai sters un produs din categorie");
        });
        b3.addActionListener(e -> { //aici fac editare de produs
            System.out.println("Ai editat un produs existent");
        });
        b4.addActionListener(e -> {//aici pun in vanzare
            System.out.println("Ai pus  un  produs la vanzare");
        });
        b5.addActionListener(e -> {//aici retrag un produs de la vanzare
            System.out.println("Ai retras un produs de la vanzare" );
        });


        pack();
        setVisible(true);
    }



    public static void main(String[] args) throws IOException {
        new Main(); }}

