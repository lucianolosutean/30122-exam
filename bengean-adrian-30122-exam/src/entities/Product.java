package entities;

import actions.RetireProduct;
import actions.SellProduct;

abstract public class Product extends ProductDetails implements SellProduct, RetireProduct {
    static boolean forSale;
    private static int categoryId;
    @Override
    abstract public void markForSale();
    abstract public void retireFromSale();
}
