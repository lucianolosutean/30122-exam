package entities;

import categories.BookCategory;
import categories.ToyCategory;
import categories.Category;

import java.util.ArrayList;

public class Site {
    private String name;
    private String adress;
    ArrayList<Category> categories=new ArrayList<>();
    public Site(String name, String adress) {
        this.name = name;
        this.adress = adress;
    }
    public void initCategories(){
        categories.add(new BookCategory() {
            @Override
            public void addProduct(Product p) {

            }

            @Override
            public Toy getProduct(int id) {
                return null;
            }
        });
        categories.add(new ToyCategory());
    }
}
