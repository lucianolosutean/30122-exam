package categories;


import entities.Product;
import actions.RetireProduct;
import entities.Toy;

abstract public class Category {
    private int id;
    private String name;
    abstract public void addProduct(RetireProduct p);

    public abstract void addProduct(Product p);

    abstract public void removeProduct(Product p);
    abstract public void updateProduct(Product p);
    abstract public Toy getProduct(int id);
}