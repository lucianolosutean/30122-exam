package categories;

import actions.RetireProduct;
import entities.Toy;
import entities.Product;

import java.util.ArrayList;

public class ToyCategory extends Category {
    private ArrayList<Toy> toys=new ArrayList<>();

    @Override
    public void addProduct(RetireProduct p) {

    }

    @Override
    public void addProduct(Product p) {
        toys.add((Toy)p);
    }

    @Override
    public void removeProduct(Product p) {
        toys.remove((Toy)p);
    }

    @Override
    public void updateProduct(Product p) {

    }

    @Override
    public Toy getProduct(int id) {
        return toys.get(id);
    }
}