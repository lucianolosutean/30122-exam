package entities;

public class Product extends ProductDetails {
    boolean forSale ;
    int categoryId ;

    public void markForSale(boolean forSale) {
        this.forSale = forSale;
    }

    public void retireFromSale(boolean forSale) {
        this.forSale = forSale;
    }
}
