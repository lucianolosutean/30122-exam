package entities;

public class Toy extends Product {
    @Override
    public void markForSale(boolean forSale) {
        super.markForSale(forSale);
    }

    @Override
    public void retireFromSale(boolean forSale) {
        super.retireFromSale(forSale);
    }
}
